# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Some articles on chemistry that might help you to develop a deep insight. I'm not a chemist; these are just some of the things I've picked up on the go. Since I've more pressing issues to attend to at the moment, this project isn't getting that much attention from my side.
* Version: 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Install a latex IDE.
* Preview the files or convert them to PDF? I dunno?

### Contribution guidelines ###

* No, I wish to write alone.

### Who do I talk to? ###

* Talk to me!
* Talk to people on chemistry stackexchange.